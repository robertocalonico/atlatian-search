# Atlatian Search


¿Qué tecnologías has usado y por qué?

```
He utilizado Boostrap 4 para el diseño de la plantilla y por su principal función que es mobile fisrt
CSS para adaptar los estilos con base a la plantilla
```

¿Se ha quedado algo fuera del proceso?

```
No he utilizado SASS ya que mi experiencia con la tencnología es mínima, es una áera de oportunidad.
Por otro lado me falto algo de diseño, algunos componentes y su comportamiento responsivo por falta de tiempo.
```

¿Has incluido algo totalmente nuevo para ti?
```
No realmente, pero definitivamente el utilizar SASS sería una excelente opción.
```

De este ejercicio, ¿qué parte te supuso un reto?
```
No había maquetado desde hace tiempo, y me confié, a la hora de comenzar no sabía por donde hacerlo, y al final de la prueba me di cuenta que estabaha haciendo todo mal y es por eso que lo volví a hacer.
```

¿Cuál fue tu parte favorita?
```
Cuando vi la plantilla me emocioné, me gustan los retos y pensé "es fácil", pero el ser grabado por mi mismo me puso algo nerviso, eso sería una experiencia nueva. Al final, el ver que tu maqueta es responsiva siempre emociona.
```

¿Te divertiste haciendo la prueba?
```
Devinitivamente, siempre es divertido crear algo nuevo.
```

¿Has aprendido algo nuevo?
```
Un poco de flex, definitivamente es algo que deví de aprender hace años.
```